package com.demo.task;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

@Service
public class TaskService {
  private List<Task> tasks = new ArrayList<>();
  private long taskIdCounter = 1;

  public List<Task> getAllTasks() {
    return tasks;
  }

  public Optional<Task> getTaskById(Long id) {
    return tasks.stream()
        .filter(task -> task.getId().equals(id))
        .findFirst();
  }

  public Task createTask(Task task) {
    task.setId(taskIdCounter++);
    tasks.add(task);
    return task;
  }

  public Optional<Task> updateTask(long id, Task updatedTask) {
    Optional<Task> existingTask = getTaskById(id);
    if (existingTask.isPresent()) {
      Task task = existingTask.get();
      task.setTitle(updatedTask.getTitle());
      task.setDescription(updatedTask.getDescription());
      task.setCompleted(updatedTask.isCompleted());
      task.setCompletionDate(updatedTask.getCompletionDate());
    }
    return existingTask;
  }

}
