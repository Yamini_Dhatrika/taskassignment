Complete Assignment Details

1. Here is the Api used for getting all tasks
GET - http://localhost:8080/tasks

2. Here is the Api used to create a task
POST - http://localhost:8080/tasks
RequestBody - 
{
    "title": "Task1",
    "description": "This is first task",
    "completed": false,
    "createdDate": "2023-10-23",
    "completionDate": ""
}

3. Here is the Api used to update a task 
PUT - http://localhost:8080/tasks/1
RequestBody - 
    {
    "title": "Task1",
    "description": "This is first task",
    "completed": true,
    "createdDate": "2023-10-23",
    "completionDate": "2023-10-23"
}

4. Here is the Api used to get a task when given its id
GET - http://localhost:8080/tasks/2

